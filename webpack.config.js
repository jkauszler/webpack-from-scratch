module.exports = function(env) {
  if (env === undefined) {
    env = "development"
  }
  return require(`./config/webpack.${env}.config.js`)
}
